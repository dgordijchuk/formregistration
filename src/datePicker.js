import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

export default ({input, label }) => (
  <div>
    <label>{label}</label>
    <DatePicker {...input}
    dateForm="MM/DD/YYYY"
    selected={input.value ? moment(input.value) : null}
    placeholderText="Date of birth"
    peekNextMonth
    showMonthDropdown
    showYearDropdown
    dropdownMode="select"
    withPortal />
  </div>
);
