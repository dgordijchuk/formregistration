import React from 'react';
import InputMask from 'react-input-mask';

export default ({input, label}) => (
  <div>
    <label>{label}</label>
    <InputMask {...input} mask="+38 (099) 999 99 99" placeholder="+38 (___) ___ __ __"  />
  </div>
);
