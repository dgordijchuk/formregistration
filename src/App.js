import React, { Component } from 'react';
import Form from './Form';


class App extends Component {
  constructor(props) {
    super(props);

  };

  submit = (values) => {
    console.log(values)
  };

  render() {
    return (
      <div className="App">
        <Form onSubmit={this.submit}></Form>
      </div>
    );
  }
}

export default App;
