import React from 'react';

export default ({ input, label, type, meta: { touched, error } }) => (
  <div className={touched && (error ? "fieldError" : "fieldSuccess")}>
    <label>{label}</label>
    <input {...input} placeholder={label} type={type} />
    {touched && (error && <span className="error">{error}</span>)}
  </div>
);
