import React from 'react'
import {Field, reduxForm} from 'redux-form'


import renderField from './renderField';
import renderDatePicker from './datePicker';
import renderPhone from './renderPhone';
import validate from './validation';

let Form = props => {
  const {handleSubmit} = props
  return (
    <form onSubmit={handleSubmit}>
      <h1>Registration form</h1>
      <div>
        <Field name="firstName" component={renderField} type="text" label="First Name"/>
      </div>
      <div>
        <Field name="lastName" component={renderField} type="text" label="Last Name"/>
      </div>
      <div>
        <Field name="username" component={renderField} type="text" label="Username"/>
      </div>
      <div>
        <Field name="email" component={renderField} type="email" label="Email"/>
      </div>
      <div>
        <Field name="password" component={renderField} type="password" label="Password"/>
      </div>
      <div>
        <Field name="dateOfBirth" component={renderDatePicker} label="Date of birth"/>
      </div>
      <div>
        <Field name="phone" component={renderPhone} label="Phone"/>
      </div>
      <div>
       <label>Gender</label>
       <div className="radio-wrapper">
         <label className="gender">
           <Field name="gender" component="input" type="radio" value="male" />
           Male
         </label>
         <label className="gender">
           <Field name="gender" component="input" type="radio" value="female" />
           Female
         </label>
       </div>
     </div>
     <div>
        <label>Chose county</label>
        <div>
          <Field name="country" component="select">
            <option />
            <option value="Bulgaria">Bulgaria</option>
            <option value="Ukraine">Ukraine</option>
            <option value="Canada">Canada</option>
            <option value="UnitedKingdom">United Kingdom</option>
          </Field>
        </div>
      </div>
      <div>
       <label>
         Agreement
         <Field
           name="agreement"
           component="input"
           type="checkbox"/>
        </label>
     </div>


      <button type="submit">Submit</button>
    </form>
  )
}

Form = reduxForm({form: 'registration', validate})(Form)

export default Form;
