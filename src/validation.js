function validate(values) {
  const errors = {}
  const {firstName='', lastName='', username, password, email} = values

  // Validation firstName field

  if (firstName.trim() === '') {
    errors.firstName = 'Invalid'
  } else if (firstName.length > 15) {
    errors.firstName = 'Must be 15 characters or less'
  } else if (firstName.length < 3)  {
    errors.firstName = 'Must be 3 characters or more'
  }

  // Validation lastName field

  if (lastName.trim() === '') {
    errors.lastName = 'Invalid'
  } else if (lastName.length > 15) {
    errors.lastName = 'Must be 15 characters or less'
  } else if (lastName.length < 3)  {
    errors.lastName = 'Must be 3 characters or more'
  }

  // Validation username field

  if (!username) {
    errors.username = 'Required'
  } else if (!/^[A-Z0-9._]+$/i.test(username)) {
    errors.username = 'Invalid username'
  }

  // Validation email field

  if (!email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = 'Invalid email address'
  }

  // Validation password field

  if (!password) {
    errors.password = 'Required'
  } else if (password.length < 6) {
    errors.password = 'Must be 6 characters or more'
  } else if (!/^[A-Z0-9]+$/i.test(password) || password === 'tralala') {
    errors.password = 'Invalid password'
  }
  return errors
}
export default validate
